"""
data augmentation for increase the number of image-set
Specialized for gamma correction
2018/06/28
Chihiro Hamada @ PAL Inc.

"""
import cv2
import numpy as np
import glob
import os
import configparser


inifile = configparser.ConfigParser()
inifile.read('./bmp_gamma/config.ini', 'UTF-8-SIG')
in_image = glob.glob(inifile.get('directory', 'in_dir') + '/*')
in_fileName = os.listdir(inifile.get('directory', 'in_dir'))


for num in range(len(in_image)):
    image = (str(in_image[num]))
    direct, filename = os.path.split(str(in_image[num]))
    name, ext = os.path.splitext(filename)

    print('direct:{}, file:{}'.format(direct, filename))
    
    if image is None:
        print("Not Open")
        continue
           
    gamma = float(inifile.get('gamma', 'gamma'))
    gamma_cvt = np.zeros((256,1),dtype = 'uint8')

    for i in range(int(inifile.get('gamma', 'arg_num'))):
        in_img = cv2.imread(image)
        gamma = gamma + float(inifile.get('gamma', 'pitch'))
        for j in range(256):
            gamma_cvt[j][0] = 255 * (float(j)/255) ** (1.0/gamma)
    
        out_img = cv2.LUT(in_img,gamma_cvt)
#       cv2.imshow("original",tmp_img)
#       cv2.imshow("gamma",img_gamma)
        out_name = inifile.get('directory', 'out_dir') + '/' + name + '_{0:03d}.bmp'.format(i)
        cv2.imwrite(out_name, out_img)
